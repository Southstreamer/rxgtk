//
// Created by hampu on 3/10/2021.
//

#include "testwindow.h"
#include <iostream>
#include <gtkmm/drawingarea.h>



TestWindow::TestWindow()
        : m_button("Generate event"), m_reset("Reset status labels"), m_box(Gtk::Orientation::ORIENTATION_VERTICAL),
        m_connect("Disconnect"), status1("Caught Button Event: False"), status2("Caught Window Event: False"),
        status3("Caught Scrolling Event: False")// creates a new button with label "Hello World".
{
    set_size_request(320, 300);
    add(m_box);

    m_button.set_size_request(80, 50);


    m_scroll_window.set_border_width(10);
    m_scroll_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    m_scroll_window.set_size_request(140, 300);

    m_scroll_window.add(m_grid);
    m_frame.add(main_box);
    m_frame.set_label("I'm a frame");
    m_frame.set_margin_end(10);
    m_frame.set_margin_start(10);
    m_frame.set_margin_bottom(10);


    status1.set_justify(Gtk::Justification::JUSTIFY_LEFT);
    status2.set_justify(Gtk::Justification::JUSTIFY_LEFT);
    status3.set_justify(Gtk::Justification::JUSTIFY_LEFT);


    main_box.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    main_box.add(m_scroll_window);
    main_box.add(status_box);

    status_box.add(status1);
    status_box.add(status2);
    status_box.add(status3);
    status_box.set_orientation(Gtk::ORIENTATION_VERTICAL);

    m_grid.set_row_spacing(30);
    m_grid.set_column_spacing(30);

    for(int i = 0; i < 1; i++){
        for(int j = 0; j < 10; j++){
            char buffer[32];
            sprintf(buffer, "button (%d,%d)\n", i, j);
            auto pButton = Gtk::make_managed<Gtk::ToggleButton>(buffer);
            m_grid.attach(*pButton, i, j);
        }
    }
    m_box2.add(m_button);
    m_box2.add(m_connect);
    m_box2.add(m_reset);
    m_box2.set_margin_start(10);
    m_box2.set_margin_top(5);
    m_box2.set_margin_bottom(5);

    m_box.add(m_box2);
    m_box.add(m_frame);

    //Bind event sources

    //Button release
    m_ButtonReleaseEvent.bind(&m_button);
    auto obs1 = m_ButtonReleaseEvent.get_observable();
    obs1.subscribe([=](GdkEventButton event){
        status1.set_text("Caught Button Event: True");
    });

    //Window configuration
    m_ConfigureEvent.bind(this);
    auto obs2 = m_ConfigureEvent.get_observable();
    obs2.subscribe([=](GdkEventConfigure event){
       status2.set_text("Caught Window Event: True");
    });

    //Scrolling
    m_scroll_window.add_events(Gdk::SCROLL_MASK);
    m_ScrollEvent.bind(&m_scroll_window);
    auto obs3 = m_ScrollEvent.get_observable();
    obs3.subscribe([=](GdkEventScroll event){
       status3.set_text("Caught Scrolling Event: True");

    });

    resetSource.bind(&m_reset);
    resetSource.get_observable().subscribe([=](GdkEventButton event){ resetStatus(); });

    connectSource.bind(&m_connect);
    connectSource.get_observable().subscribe([=](GdkEventButton event){ if(isConnected){
        m_connect.set_label("Reconnect");
        disconnectAll();

    }
    else{
        m_connect.set_label("Disconnect");
        connectAll();
    }});

    show_all_children();

}
void TestWindow::resetStatus() {
    status1.set_text("Caught Button Event: False");
    status2.set_text("Caught Window Event: False");
    status3.set_text("Caught Scroll Event: False");
}
void TestWindow::connectAll() {
    std::cout << "Reconnecting" << std::endl;
    isConnected = true;
    m_ScrollEvent.reconnect();
    m_ScrollEvent.get_observable().subscribe([=](GdkEventScroll event){
        status3.set_text("Caught Scrolling Event: True");

    });
    m_ButtonReleaseEvent.reconnect();
    m_ButtonReleaseEvent.get_observable().subscribe([=](GdkEventButton event){
        status1.set_text("Caught Button Event: True");
    });

    m_ConfigureEvent.reconnect();
    m_ConfigureEvent.get_observable().subscribe([=](GdkEventConfigure event){
        status2.set_text("Caught Window Event: True");
    });


}
void TestWindow::disconnectAll() {
    std::cout << "Disconnecting" << std::endl;
    isConnected = false;
    m_ScrollEvent.disconnect();
    m_ButtonReleaseEvent.disconnect();
    m_ConfigureEvent.disconnect();
}

