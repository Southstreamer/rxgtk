
#include <regex>
#include <random>
#include "testwindow.h"

int main(int argc, char *argv[])
{

    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");


    TestWindow window;

    return app->run(window);
}