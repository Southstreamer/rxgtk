#pragma once


#include "gtkeventsource.h"


class TestWindow : public Gtk::Window
{
private:
    //EventSources

    //Scrolling
    ScrollEventSource m_ScrollEvent;

    //Window size
    ConfigureEventSource m_ConfigureEvent;

    //Button release
    ButtonReleaseEventSource m_ButtonReleaseEvent;

    //Button press
    ButtonPressEventSource m_ButtonPressEvent;

    ButtonReleaseEventSource releaseSource;



    ButtonReleaseEventSource resetSource;
    ButtonReleaseEventSource connectSource;


    bool isConnected = true;
    void resetStatus();
    void connectAll();
    void disconnectAll();

public:
    TestWindow();

protected:

    //Member widgets:
    Gtk::Button m_button;
    Gtk::Box m_box;
    Gtk::Box m_box2;
    Gtk::Frame m_frame;
    Gtk::Grid m_grid;
    Gtk::ScrolledWindow m_scroll_window;

    Gtk::Box status_box;
    Gtk::Box main_box;
    Gtk::Button m_connect;
    Gtk::Button m_reset;

    //Test status labels
    Gtk::Label status1;
    Gtk::Label status2;
    Gtk::Label status3;
};