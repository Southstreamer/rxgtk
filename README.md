# Usage

__RxGTK+__ is a header C++ library that depends on the [Gtkmm3](https://www.gtkmm.org/en/download.html) and [RxCpp](https://github.com/ReactiveX/RxCpp). Currently in quite an early stage.

# Example

```cpp

#include rxgtk/gtkeventsource.h

class ExampleWindow: public Gtk::Window
{
	private:
	
		//Source for button release events
		ButtonReleaseEventSource m_source;
		
	public:
		ExampleWindow();
		
	protected:
		//Widgets
		Gtk::Button m_button;

}
ExampleWindow::ExampleWindow() : m_button("This is a button")
{
	set_size_request(400, 300);
	add(m_button);
	
	m_source.bind(&m_button);
	m_source.get_observable().subscribe([](GdkEventButton event){
		std::cout << "Button pressed" << std::endl;
	});
	show_all_children();
}


int main(int argc, char * argv[])
{
	auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
	
	ExampleWindow window;
	
	return app->run(demoWindow);
}
```

# Installing
To use RxGTK+ you need to install both [Gtkmm3](https://www.gtkmm.org/en/download.html) and [RxCpp](https://github.com/ReactiveX/RxCpp).
Once installed just move the __rxgtk__ folder into your include directory

# Tests
The repository contains a __test__ directory which includes the files to run the testing application.


