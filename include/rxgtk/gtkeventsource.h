#ifndef GTKMMPROJECT_EVENT_SOURCE_H
#define GTKMMPROJECT_EVENT_SOURCE_H

#include "rxcpp/rx.hpp"
#include <gtkmm.h>

/**
 * Abstract EventSource class (Inherit to create any X-Event event source)
 * @tparam EventType - type of events that should be processed by the source
 */
template<typename T>
class EventSource{
private:
    rxcpp::rxsub::subject<T> subject; ///< RxCpp subject to efficiently manage emission of signals


    bool consume = false; ///< bool to indicate if the event should be consumed and not propagate further

    /**
     *  Disconnect from event feed
     */
    void _disconnect(){
        if(connection.connected())
            connection.disconnect();
    }

protected:


    Gtk::Widget* widget; ///< The main window which captures the events (Widget will take this spot / added)


    sigc::connection connection;  ///< Managable connection object

    /**
     * Signal handler
     */
    bool catch_event(T* event){
        subject.get_subscriber().on_next(*event);
        return consume;
    };

public:
    /**
     * Constructor for adding of disconnection method
     */
    EventSource(){
        subject.get_subscription().add([=](){
            _disconnect();
        });
    }

    /**
     * Virtual method bind, needs to be implemented in each concrete class
     * @param widget - Gtk::Widget to bind
     * @param after  - boolean value deciding to run signal handler after other handlers
     */
    virtual void bind(Gtk::Widget* widget, bool after) = 0;

    /**
     *  When a subject has stopped, a new one needs to be created to work again
     */
    virtual void reconnect(){
        subject = rxcpp::rxsub::subject<T>();

        subject.get_subscription().add([=](){
            _disconnect();
        });
    }

    /**
     *  Set if events should be consumed and not processed further, or not (default is FALSE)
     */

    void set_consumption(bool consume){
        this->consume = consume;
    }

    /**
     * Disconnects all the subscribers
     */
    void disconnect(){
        subject.get_subscription().unsubscribe();
    }

    /**
     * Method to return an observable
     * @return rxpcpp::observable<T>
     */
    rxcpp::observable<T> get_observable(){
        return subject.get_observable();
    }
};


/**
 * Concrete classes
 */


class ButtonPressEventSource : public EventSource<GdkEventButton>{
public:

    /**
     * Bind implementation - Set event masks if needed and connect the signal handler to the correct SignalProxy
     * @param after - bool indicating if the signals should be prioritized by this source in comparison to other
     * signal handlers bound to the widget
     */
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_button_press_event().connect(sigc::mem_fun(this, &ButtonPressEventSource::catch_event), after);
    }

    /**
     * When implementing virtual method, the base class method needs to be called here as well
     * @param after - bool indicating if the signals should be prioritized by this source in comparison to other
     * signal handlers bound to the widget
     */
    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_button_press_event().connect(sigc::mem_fun(this, &ButtonPressEventSource::catch_event), after);
    }
};


/**
 * Below are identical structures as of the concrete above except for other various event types. The same comments
 * mentioned above apply for further subclassing
 */

class KeyPressEventSource : public EventSource<GdkEventKey>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_key_press_event().connect(sigc::mem_fun(this, &KeyPressEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_key_press_event().connect(sigc::mem_fun(this, &KeyPressEventSource::catch_event), after);
    }
};

class KeyReleaseEventSource : public EventSource<GdkEventKey>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_key_release_event().connect(sigc::mem_fun(this, &KeyReleaseEventSource::catch_event), after);
    }
    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_key_release_event().connect(sigc::mem_fun(this, &KeyReleaseEventSource::catch_event), after);
    }
};


class ConfigureEventSource : public EventSource<GdkEventConfigure>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_configure_event().connect(sigc::mem_fun(this, &ConfigureEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_configure_event().connect(sigc::mem_fun(this, &ConfigureEventSource::catch_event), after);
    }
};

class ButtonReleaseEventSource : public EventSource<GdkEventButton>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_button_release_event().connect(sigc::mem_fun(this, &ButtonReleaseEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_button_release_event().connect(sigc::mem_fun(this, &ButtonReleaseEventSource::catch_event), after);
    }

};

class ScrollEventSource : public EventSource<GdkEventScroll>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_scroll_event().connect(sigc::mem_fun(this, &ScrollEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_scroll_event().connect(sigc::mem_fun(this, &ScrollEventSource::catch_event), after);
    }
};

class MotionNotifyEventSource : public EventSource<GdkEventMotion> {
public:


    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_motion_notify_event().connect(sigc::mem_fun(this, &MotionNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_motion_notify_event().connect(sigc::mem_fun(this, &MotionNotifyEventSource::catch_event), after);
    }
};

class DeleteEventSource : public EventSource<GdkEventAny>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_delete_event().connect(sigc::mem_fun(this, &DeleteEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_delete_event().connect(sigc::mem_fun(this, &DeleteEventSource::catch_event), after);
    }
};

class FocusInEventSource : public EventSource<GdkEventFocus>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_focus_in_event().connect(sigc::mem_fun(this, &FocusInEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_focus_in_event().connect(sigc::mem_fun(this, &FocusInEventSource::catch_event), after);
    }
};

class FocusOutEventSource : public EventSource<GdkEventFocus>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_focus_out_event().connect(sigc::mem_fun(this, &FocusOutEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_focus_out_event().connect(sigc::mem_fun(this, &FocusOutEventSource::catch_event), after);
    }
};

class MapEventSource : public EventSource<GdkEventAny>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_map_event().connect(sigc::mem_fun(this, &MapEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_map_event().connect(sigc::mem_fun(this, &MapEventSource::catch_event), after);
    }
};


class EnterNotifyEventSource : public EventSource<GdkEventCrossing>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_enter_notify_event().connect(sigc::mem_fun(this, &EnterNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_enter_notify_event().connect(sigc::mem_fun(this, &EnterNotifyEventSource::catch_event), after);
    }
};

class LeaveNotifyEventSource : public EventSource<GdkEventCrossing>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_leave_notify_event().connect(sigc::mem_fun(this, &LeaveNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_leave_notify_event().connect(sigc::mem_fun(this, &LeaveNotifyEventSource::catch_event), after);
    }
};

class UnmapEventSource : public EventSource<GdkEventAny>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_unmap_event().connect(sigc::mem_fun(this, &UnmapEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_unmap_event().connect(sigc::mem_fun(this, &UnmapEventSource::catch_event), after);
    }
};

class PropertyNotifyEventSource : public EventSource<GdkEventProperty>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_property_notify_event().connect(sigc::mem_fun(this, &PropertyNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_property_notify_event().connect(sigc::mem_fun(this, &PropertyNotifyEventSource::catch_event), after);
    }
};

class SelectionClearEventSource : public EventSource<GdkEventSelection>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_selection_clear_event().connect(sigc::mem_fun(this, &SelectionClearEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_selection_clear_event().connect(sigc::mem_fun(this, &SelectionClearEventSource::catch_event), after);
    }
};

class SelectionRequestEventSource : public EventSource<GdkEventSelection>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_selection_request_event().connect(sigc::mem_fun(this, &SelectionRequestEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_selection_request_event().connect(sigc::mem_fun(this, &SelectionRequestEventSource::catch_event), after);
    }
};

class SelectionNotifyEventSource : public EventSource<GdkEventSelection>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_selection_notify_event().connect(sigc::mem_fun(this, &SelectionNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_selection_notify_event().connect(sigc::mem_fun(this, &SelectionNotifyEventSource::catch_event), after);
    }
};

class ProximityInEventSource : public EventSource<GdkEventProximity>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_proximity_in_event().connect(sigc::mem_fun(this, &ProximityInEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_proximity_in_event().connect(sigc::mem_fun(this, &ProximityInEventSource::catch_event), after);
    }
};

class ProximityOutEventSource : public EventSource<GdkEventProximity>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_proximity_out_event().connect(sigc::mem_fun(this, &ProximityOutEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_proximity_out_event().connect(sigc::mem_fun(this, &ProximityOutEventSource::catch_event), after);
    }
};

class VisibilityNotifyEventSource : public EventSource<GdkEventVisibility>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_visibility_notify_event().connect(sigc::mem_fun(this, &VisibilityNotifyEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_visibility_notify_event().connect(sigc::mem_fun(this, &VisibilityNotifyEventSource::catch_event), after);
    }
};

class WindowStateEventSource : public EventSource<GdkEventWindowState>{
public:
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_window_state_event().connect(sigc::mem_fun(this, &WindowStateEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_window_state_event().connect(sigc::mem_fun(this, &WindowStateEventSource::catch_event), after);
    }
};

class DamageEventSource : public EventSource<GdkEventExpose>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_damage_event().connect(sigc::mem_fun(this, &DamageEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_damage_event().connect(sigc::mem_fun(this, &DamageEventSource::catch_event), after);
    }
};

class TouchEventSource : public EventSource<GdkEventTouch>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_touch_event().connect(sigc::mem_fun(this, &TouchEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_touch_event().connect(sigc::mem_fun(this, &TouchEventSource::catch_event), after);
    }
};

class GrabBrokenEventSource : public EventSource<GdkEventGrabBroken>{
    void bind(Gtk::Widget* widget, bool after = false){
        this->widget = widget;
        connection = widget->signal_grab_broken_event().connect(sigc::mem_fun(this, &GrabBrokenEventSource::catch_event), after);
    }

    void reconnect(bool after = false){
        EventSource::reconnect();
        connection = widget->signal_grab_broken_event().connect(sigc::mem_fun(this, &GrabBrokenEventSource::catch_event), after);
    }
};
#endif //GTKMMPROJECT_EVENT_SOURCE_H
