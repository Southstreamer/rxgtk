#pragma once


#include "gtkeventsource.h"


class DemoWindow : public Gtk::Window
{
private:
    //EventSources

    //Scrolling
    ScrollEventSource m_ScrollEvent;

    //Window size
    ConfigureEventSource m_ConfigureEvent;

    //Button release
    ButtonReleaseEventSource m_ButtonReleaseEvent;


public:
    DemoWindow();

protected:

    //Member widgets:
    Gtk::Button m_button;
    Gtk::Entry m_entry;
    Gtk::Label m_label;
    Gtk::Box m_box;
    Gtk::Box m_box2;
    Gtk::Frame m_frame;
    Gtk::Grid m_grid;
    Gtk::ScrolledWindow m_scroll_window;
};