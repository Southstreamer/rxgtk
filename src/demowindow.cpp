//
// Created by hampu on 3/10/2021.
//

#include "demowindow.h"
#include <iostream>
#include <gtkmm/drawingarea.h>



DemoWindow::DemoWindow()
        : m_button("This is a button"), m_label("This is a label"), m_box(Gtk::Orientation::ORIENTATION_VERTICAL)   // creates a new button with label "Hello World".
{
    set_size_request(400, 300);
    add(m_box);
    m_entry.set_max_length(50);
    m_entry.set_text("hello");
    m_entry.set_text(m_entry.get_text() + " world");
    m_entry.select_region(0, m_entry.get_text_length());

    m_button.set_size_request(80, 50);


    m_scroll_window.set_border_width(10);
    m_scroll_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    m_scroll_window.set_size_request(400, 300);

    m_scroll_window.add(m_grid);
    m_frame.add(m_scroll_window);
    m_frame.set_label("I'm a frame");
    m_frame.set_margin_end(10);
    m_frame.set_margin_start(10);
    m_frame.set_margin_bottom(10);

    m_grid.set_row_spacing(30);
    m_grid.set_column_spacing(30);

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 10; j++){
            char buffer[32];
            sprintf(buffer, "button (%d,%d)\n", i, j);
            auto pButton = Gtk::make_managed<Gtk::ToggleButton>(buffer);
            m_grid.attach(*pButton, i, j);
        }
    }
    m_box2.add(m_button);
    m_box2.add(m_label);
    m_box2.set_margin_start(10);
    m_box2.set_margin_top(5);
    m_box2.set_margin_bottom(5);
    m_label.set_text("Label");
    m_label.set_margin_start(10);

    m_box.add(m_entry);
    m_box.add(m_box2);
    m_box.add(m_frame);
    std::cout << "Main thread: " << std::this_thread::get_id() << std::endl;

    //Bind event sources

    //Button release
    m_ButtonReleaseEvent.bind(&m_button);
    auto obs1 = m_ButtonReleaseEvent.get_observable();
    obs1.subscribe([=](GdkEventButton event){
        m_label.set_text(m_entry.get_text());
    });

    //Window configuration
    m_ConfigureEvent.bind(this);
    auto obs2 = m_ConfigureEvent.get_observable();
    obs2.subscribe([=](GdkEventConfigure event){
       std::string text = "Window position: [X: " + std::to_string(event.x) + ", Y: " + std::to_string(event.y) + "]";
       m_label.set_text(text);
    });

    //Scrolling
    m_scroll_window.add_events(Gdk::SCROLL_MASK);
    m_ScrollEvent.bind(&m_scroll_window);
    auto obs3 = m_ScrollEvent.get_observable();
    obs3.subscribe([=](GdkEventScroll event){
        std::string text;
        if(event.direction == GDK_SCROLL_DOWN){
            text = "Scrolling downwards!";
        }
        else if(event.direction == GDK_SCROLL_UP){
            text = "Scrolling upwards!";
        }
        m_label.set_text(text);
    });

    show_all_children();

}

