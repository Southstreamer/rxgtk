
#include <regex>
#include <random>
#include "demowindow.h"

int main(int argc, char *argv[])
{

    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    DemoWindow demoWindow;

    return app->run(demoWindow);
}